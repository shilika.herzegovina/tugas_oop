<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new animal("shaun");
echo "Name :" . $object->name ."<br>";
echo "Leg :" . $object->leg ."<br>";
echo "Cold_blooded :" . $object -> cold_blooded ."<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Name :" . $kodok->name ."<br>";
echo "Leg :" . $kodok->leg ."<br>";
echo "Cold_blooded :" . $kodok-> cold_blooded ."<br>";
$kodok->jump() ; "hop hop";
echo "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->name ."<br>";
echo "Leg :" . $sungokong->leg ."<br>";
echo "Cold_blooded :" . $sungokong-> cold_blooded ."<br>";
$sungokong->yell() ; "Auooo"

?>